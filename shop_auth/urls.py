from django.urls import path

from .views import shop_login_page, user_logout

urlpatterns = [
    path('login/', shop_login_page),
    path('logout/', user_logout),
]