from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout

# Create your views here.

@csrf_protect
def shop_login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponse(f'Welcome {user.username}')
    return render(request, 'shop_auth/login.html')

def user_logout(request):
    logout(request)
    return HttpResponse('You are logged out')

