>>> from articles.models import Article
>>> user = User.objects.filter(username='user')
>>> user
<QuerySet []>
>>> user = User.objects.filter(username='user2')
>>> user
<QuerySet [<User: user2>]>
>>> user = User.objects.filter(username='user3')
>>> user
<QuerySet [<User: user3>]>
>>> user = User.objects.filter(username__icontains='user')
>>> user
<QuerySet [<User: user2>, <User: user3>]>
>>> user = User.objects.filter(first_name__icontains='user')
>>> user
<QuerySet []>
>>> user = User.objects.filter(first_name__icontains='vam')
>>> user
<QuerySet [<User: vama>]>
>>> user = User.objects.filter(first_name__icontains='sen')
>>> user
<QuerySet [<User: senthil>]>
>>> user = User.objects.filter(first_name__icontains='hil')
>>> user
<QuerySet [<User: senthil>]>
>>> user = User.objects.filter(first_name__icontains='e')
>>> user
<QuerySet [<User: senthil>]>