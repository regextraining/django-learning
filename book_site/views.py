from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect



@csrf_protect
def say_hello(request):
    name = request.GET.get('name', 'Marathi Manus')
    # get ip from request
    full_name = name + ' ' + 'Shah'
    return render(request, 'books_site/basic.html', {'name': full_name, 'semester': '5th'})

@csrf_protect
def say_bye(request):
    return HttpResponse('Khatam Tata Bye Bye!')
