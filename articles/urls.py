from django.urls import path
from .views import show_articles, show_user_article, show_logged_in_user_articles

urlpatterns = [
    path('all/', show_articles),
    path('user_article/', show_user_article),
    path('logged_in_user_article/', show_logged_in_user_articles),
]