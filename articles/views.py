from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User

from .models import Article

# Create your views here.

@csrf_protect
def show_articles(request):
    articles = Article.objects.all()
    print(articles)
    return  render(request, 'articles/show_article.html', {'articles': articles})

def show_user_article(request):
    username = request.GET.get('username')
    user = User.objects.get(username=username)
    user_articles = Article.objects.filter(author=user)
    return render(
        request, 'articles/show_article.html', {'articles': user_articles})

def show_logged_in_user_articles(request):
    user = request.user
    user_articles = Article.objects.filter(author=user)
    return render(
        request, 'articles/show_article.html', {'articles': user_articles})
