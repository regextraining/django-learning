from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone



# Create your models here.
class Article(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    headline = models.CharField(max_length=250, null=True, blank=True)
    paragraph = models.CharField(max_length=1000, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self) -> str:
        return f'{self.paragraph}'

    # def delete(self, using: Any = ..., keep_parents: bool = ...) -> Tuple[int, Dict[str, int]]:
    #     return super().delete(using, keep_parents)

# >>> from articles.models import Article
# >>> from django.contrib.auth.models import User
# >>> articles = Article.objects.all()
# >>> articles
# <QuerySet [<Article: Reinforcement Learning>, <Article: Django Authentication>, <Article: Python Decorators>]>
# >>> users = User.objects.all()
# >>> users
# <QuerySet [<User: shivank>, <User: user2>, <User: user3>]>
# >>> 
# KeyboardInterrupt
